# Nightwatch 

### Basics
Saucelabs runs tests in cloud, doesn't require drivers or selenium to be run locally.

### Credentials 
Requires credentials to use Saucelabs, stored in ```nightwatch.json```.

### Running tests
Runs with ```npm run nightwatch```

- this will run the ```title.advising.js``` test found in ```/tests```

