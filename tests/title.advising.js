module.exports = {

  '@tags': ['accessibility'],

  'Title and Accessibility Test': function(browser) { 

    // Browser is the browser that is being controlled
    browser
      .url('https://www.wwu.edu/advising') // Navigate to the url
      .waitForElementVisible('body', 1000) // Wait until you can see the body element.
      .verify.title('Academic Advising Center') // Verify that the title is 'Bing'

      .initAccessibility()
      .assert.accessibility('html', { verbose: true })

      .end() // This must be called to close the browser at the end
    }
  }
