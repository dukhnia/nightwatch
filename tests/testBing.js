/**
 * @file Run Axe accessibility tests with Nightwatch.
 */

const axeOptions = { 
    timeout: 500,
    runOnly: {
      type: 'tag',
      values: ['wcag2a', 'wcag2aa'],
    },
    reporter: 'v2',
    elementRef: true,
  };

  module.exports = {
    'Accessibility test': (browser) => {
      browser
        .url(`${browser.launch_url}/styleguide/section-6.html`)
        .pause(1000)
        .initAccessibility()
        .assert.accessibility('.kss-modifier__example', axeOptions)
        .end();
    },
  };
